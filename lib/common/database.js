const mongoose = require('mongoose')

const username = 'hendrix'
const password = 'asd'
const server = 'cluster0-8gnjr.mongodb.net'
const database = 'demo?retryWrites=true&w=majority'

class Database {
  constructor () {
    this._connect()
  }

  _connect () {
    mongoose.connect(`mongodb+srv://${username}:${password}@${server}/${database}`)
      .then(() => {
        console.log('Database connection successful')
      })
      .catch(err => {
        console.error('Database connection error')
      })
  }
}

module.exports = new Database()
