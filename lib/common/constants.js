const constants = {
  guttenberg_url: 'http://www.gutenberg.org/files/61217/61217.txt',
  anagram_url: 'https://raw.githubusercontent.com/dwyl/english-words/master/words_dictionary.json'
}

module.exports = constants
