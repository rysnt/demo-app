const guttenbergPangram = {
  sentences: [],
  result: [],
  joinString: '',
  run: async function () {
    this.sentences.forEach((v) => {
      this.joinString += v + '.'
      if (this.isPangram(this.joinString)) {
        this.result.push(this.joinString)
        this.joinString = ''
      }
    })
    return this.result
  },

  isPangram: function (sentence) {
    const characters = sentence.replace(/\W/g, '').toLocaleLowerCase().split('')

    const output = characters.filter(function (x, pos) {
      return characters.indexOf(x) === pos
    })

    if (output.length === 26) {
      return true
    } else {
      return false
    }
  }

}

module.exports = guttenbergPangram
