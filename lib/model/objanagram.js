const Mongoose = require('mongoose')
const Schema = Mongoose.Schema

const internals = {}

internals.ObjAnagramSchema = new Schema({
  key: String,
  word: String
})

module.exports = Mongoose.model('anagram', internals.ObjAnagramSchema)
