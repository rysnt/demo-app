const anagramFile = {
    tempText: [],
    result: [],
    splitText: async function (text) {
        this.tempText = Object.keys(text)
    },
    sortingText: function () {
        for(let i=0; i<this.tempText.length; i++) {
            let textIndex = this.tempText[i].toLowerCase()
            let tempObject = {
                "key": textIndex.split("").sort().join(""),
                "word": textIndex
            }
            this.result.push(tempObject);
        }
        return this.result
    },
  }
  
  module.exports = anagramFile
  