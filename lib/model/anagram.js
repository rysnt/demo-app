const axios = require('axios')
const httpHeader = require('axios/lib/adapters/http')
const anagramFile = require('./anagram_file')
const ObjAnagram = require('./objanagram')

const internals = {}

class Anagram {
  constructor () {
    let { instance } = internals
    if (!instance) {
      instance = this
    }
    return instance
  }

  post (payload) {
    const promise = new Promise((resolve, reject) => {
      axios.get(payload.source, {
        responseType: 'stream',
        adapter: httpHeader
      }).then((response) => {
        const stream = response.data
        stream.on('data', (chunk) => {
          const buffer = Buffer.from(chunk)
          anagramFile.splitText(buffer.toString('utf8'))
        })
        stream.on('end', () => {
          const result = anagramFile.sortingText();
          resolve(result);
        })
      })
    })
    return promise
  }

  checkAnagram(str1, str2) {
    const hist = {}
    for (let i = 0; i < str1.length; i++) {
      const char = str1[i]
      if (hist[char]) {
        hist[char]++
      } else {
        hist[char] = 1
      }
    }
  
    for (let j = 0; j < str2.length; j++) {
      const char = str2[j]
      if (hist[char]) {
        hist[char]--
      } else {
        return false;
      }
    }
  
    return true;
  }

  checkDictionary(text) {
    const promise = new Promise( async (resolve, reject) => {
      ObjAnagram.findOne({"word": text}).exec(function (err, result) {
        resolve(result)
      })
    })
    return promise
  }
  
  createAnagram (payload) {
    const promise = new Promise((resolve, reject) => {
      axios.get(payload.source).then((response) => {
        anagramFile.splitText(response.data);
        const result = anagramFile.sortingText();
        resolve(result);
      })
    })
    return promise
  }

  saveAnagram (results) {
    const promise = new Promise((resolve, reject) => {

      ObjAnagram.collection.insert(results, function (err, docs) {
        if (err){ 
            return console.error(err);
        } else {
          console.log("Multiple documents inserted to Collection");
        }
      }); 
    })
    return promise
  }

  getRandomAnagram () {
    const promise = new Promise((resolve, reject) => {
      ObjAnagram.count().exec(function (err, count) {
        const random = Math.floor(Math.random() * count)
        ObjAnagram.findOne().skip(random).exec(function (err, result) {
          resolve(result)
        })
      })
    })
    return promise
  }
}

module.exports = Anagram
