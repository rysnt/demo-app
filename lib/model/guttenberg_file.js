const guttenbergFile = {
  tempText: [],
  addLine: function (text) {
    const newText = text.replace(/(\r\n|\n|\r)/gm, ' ').replace(/[0-9]/g, '')
    this.tempText.push(newText)
  },
  joinContentText: async function () {
    return this.tempText.join(' ')
  },
  addSentencesArray: async function (text) {
    sentences = text.split('.')
    sentences = sentences.filter(v => v != '')
    sentences = sentences.map(v => v.trim())
    return sentences
  }
}

module.exports = guttenbergFile
