const ObjAnagram = require('./objanagram')

const internals = {}

class Isogram {
    constructor () {
        let { instance } = internals
        if (!instance) {
        instance = this
        }
        return instance
    }

    isIsogram(str){ 
        return !/(\w).*\1/i.test(str)
    }
}

module.exports = Isogram
