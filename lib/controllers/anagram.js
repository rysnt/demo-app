const Boom = require('@hapi/boom')
const Joi = require('@hapi/joi')

const Anagram = require('../model/anagram')

const anagram = new Anagram()

const internals = {}

internals.validate = {
  handler: async (request, reply) => {
    try {
      const sourcePayload = request.payload
      const sourceJsonPayload = JSON.parse(JSON.stringify(sourcePayload))
      var isAnagram = await anagram.checkAnagram(sourceJsonPayload.text1, sourceJsonPayload.text2)
      if(isAnagram){
        var result = await anagram.checkDictionary(sourceJsonPayload.text2)
      }
      return result
    } catch (err) {
      return Boom.badRequest(err)
    }
  },
  validate: {
    payload: Joi.object({
        text1: Joi.string(),
        text2: Joi.string(),
    })
  },
  description: 'Check anagram',
  notes: 'Pass json as a request body; example : {"text1": "silent", text2: "listen"}',
  tags: ['api', 'anagram'],
}

internals.create = {
  handler: (request, reply) => {
    try {
      const sourcePayload = request.payload
      const sourceJsonPayload = JSON.parse(JSON.stringify(sourcePayload))
      anagram.createAnagram(sourceJsonPayload).then((result) => {
        if (result.length > 0) {
          anagram.saveAnagram(result)
        }
      })
      request = { status: 'In Progress', code: 0 }
      return request;
    } catch (err) {
      return Boom.badRequest(err)
    }
  },
  validate: {
    payload: Joi.object({
        source: Joi.string(),
    })
  },
  description: 'Generate anagram from url site picked',
  notes: 'Pass json as a request body; example : {"source": "https://raw.githubusercontent.com/dwyl/english-words/master/words_dictionary.json"}',
  tags: ['api', 'anagram'],
}

internals.getRandom = {
  handler: async (request, reply) => {
    const result = await anagram.getRandomAnagram()
    return result
  },
  description: 'Get random anagram from server',
  notes: 'Get random anagram from server',
  tags: ['api', 'anagram'],
}

module.exports = internals
