const Boom = require('@hapi/boom')
const Joi = require('@hapi/joi')

const Isogram = require('../model/isogram')

const isogram = new Isogram()

const internals = {}

internals.validate = {
  handler: async (request, reply) => {
    try {
      const sourcePayload = request.payload
      const sourceJsonPayload = JSON.parse(JSON.stringify(sourcePayload))
      
      const isIsogram = await isogram.isIsogram(sourceJsonPayload.text)
      if(isIsogram == sourceJsonPayload.isIsogram){
          return true
      } else {
          return false
      }
    } catch (err) {
      return Boom.badRequest(err)
    }
  },
  validate: {
    payload: Joi.object({
        text: Joi.string(),
        isIsogram: Joi.boolean()
    })
  },
  description: 'Check isogram valid or not',
  notes: 'Pass json as a request body; example : {"text": "example"}',
  tags: ['api', 'isogram'],
}

module.exports = internals
