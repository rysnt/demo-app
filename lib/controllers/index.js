const Path = require('path')
const Fs = require('fs')

const internals = {}

Fs.readdirSync(__dirname).forEach((file) => {
  if (file === 'index.js') {
    return
  }

  let ctrlName = Path.basename(file, '.js')
  ctrlName = ctrlName.charAt(0).toUpperCase + ctrlName.substr(1).toLowerCase()

  internals[ctrlName] = require(Path.join(__dirname, file))
})

module.exports = internals
