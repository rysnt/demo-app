const pangramCtrl = require('../controllers/pangram')

const internals = {}

internals.routes = [
  { method: 'POST', path: '/pangram-post', options: pangramCtrl.post},
  { method: 'GET', path: '/pangram-random', options: pangramCtrl.getRandom }
]

module.exports = internals.routes
