const mainCtrl = require('../controllers/main')

const internals = {}

internals.routes = [
  { method: 'GET', path: '/', options: mainCtrl.home }
]

module.exports = internals.routes
