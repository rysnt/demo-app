const anagramCtrl = require('../controllers/anagram')

const internals = {}

internals.routes = [
  { method: 'POST', path: '/anagram-create', options: anagramCtrl.create },
  { method: 'GET', path: '/anagram-random', options: anagramCtrl.getRandom },
  { method: 'POST', path: '/anagram-validate', options: anagramCtrl.validate }
]

module.exports = internals.routes
