const Path = require('path')
const Fs = require('fs')

const internals = {}

Fs.readdirSync(__dirname).forEach((file) => {
  if (file === 'index.js') {
    return;
  }

  const ctrlName = Path.basename(file, '.js');

  internals[ctrlName] = require(Path.join(__dirname, file));
})
const exposeRoutes = []
Object.keys(internals).forEach((routeKey) => {
  internals[routeKey].forEach((value) => {
    exposeRoutes.push(value);
  })
})

module.exports = exposeRoutes
