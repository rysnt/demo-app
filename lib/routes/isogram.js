const isogramCtrl = require('../controllers/isogram')

const internals = {}

internals.routes = [
  { method: 'POST', path: '/isogram-validate', options: isogramCtrl.validate }
]

module.exports = internals.routes
